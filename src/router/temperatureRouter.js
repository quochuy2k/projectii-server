const express = require('express')
const {
    getAll,
    paginationTemp
} = require('../controller/temperatureController')
const {
    updateStateLed
} = require('../controller/ledController')

const router = express.Router();
router.route('/temperature/:limit')
    .get(getAll)
router.route('/temperature/:activePage')
    .get(paginationTemp)
router.route('/led')
    .post(updateStateLed)
module.exports = router